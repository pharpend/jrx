
const announceMsg =
    {type : "to_aepp",
     data : {"jsonrpc" : "2.0",
             "method"  : "connection.announcePresence",
             "params"  : {"id"        : "jr_0.1.0@psychobitch.party",
                          "name"      : "Jaeck Russell",
                          "networkId" : "ae_uat",
                          "origin"    : browser.runtime.getURL(""),
                          "type"      : "extension"}}};

/**
 * No idea why this works
 *
 * See
 * https://stackoverflow.com/questions/951021/what-is-the-javascript-version-of-sleep
 */
async function
sleep
    (ms)
{
    return new Promise((resolve) => setTimeout(resolve, ms));
}


/**
 * announce presence every 3 seconds
 */
async function
announceLoop
    ()
{
    while (true)
    {
        window.postMessage(announceMsg);
        await sleep(3000);
    }
}

announceLoop();

//window.postMessage(announceMsg);
////console.log('poop');
//console.log(announceMsg);
